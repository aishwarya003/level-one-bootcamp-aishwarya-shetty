//WAP to find the volume of a tromboloid using 4 functions.
// Online C compiler to run C program online
#include <stdio.h>
//function for taking input
int input()
{
    int num;
    scanf("%d",&num);
    return num;
}
//function to compute volume
float compute(int h, int b, int d)
{
    float ans;
    ans = 0.333*((h*b*d)+(d/b)); //1/3 = 0.333...
    return ans;
}
//function to print
void print(float vol)
{
     printf("Volume of tromboloid is %.3f",vol); //rounding of the answer to 3 decimal places
}
int main() {
    int h,b,d;
    float vol = 0.0;
	//taking input
    printf("Enter h :\n");
    h = input();
     printf("Enter b :\n");
    b = input();
    printf("Enter d :\n");
    d = input();
	//computing volume
    vol = compute(h,b,d);
	//printing
    print(vol);
    return 0;
}