//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include<math.h>
int input()
{
    int a;
    scanf("%d",&a);
    return a;
}
float distance(int x1, int y1, int x2, int y2)
{
    float ans = sqrt(pow((x2 - x1),2)+pow((y2 - y1),2));
    return ans;
}
void print(int x1, int y1, int x2, int y2, float ans)
{
    printf("Distance between (%d,%d) and (%d,%d) is %.3f",x1,y1,x2,y2,ans);
}
int main() {
    printf("Enter x1\n");
    int x1 = input();
    printf("Enter y1\n");
    int y1 = input();
    printf("Enter x2\n");
    int x2 = input();
    printf("Enter y2\n");
    int y2 = input();
    float dist = distance(x1,y1,x2,y2);
    print(x1,y1,x2,y2,dist);
    return 0;
}