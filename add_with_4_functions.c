//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
//function for taking input
int input()
{
    int num;
    scanf("%d",&num);
    return num;
}
//function to compute sum
int compute(int a, int b)
{
    int ans;
    ans = a + b;
    return ans;
}
//function to print
void print(int a,int b,int ans)
{
     printf("Addition of %d and %d is %d",a,b,ans);
}
int main() {
    int a,b,sum;
    printf("Enter number 1:\n");
    a = input();
     printf("Enter number 2 :\n");
    b = input();
    sum = compute(a,b);
    print(a,b,sum);
    return 0;
}