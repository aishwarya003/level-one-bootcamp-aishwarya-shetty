//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>
int input(int n, int a[n])
{
    for(int i = 0;i < n;i++)
        scanf("%d",&a[i]);
}
int findSum(int n,int a[n])
{
    int sum = 0;
    for(int i = 0;i < n;i++)
        sum = sum + a[i];
    return sum;    
}
void print(int n,int a[n],int sum)
{
     printf("Sum of ");
    for(int i = 0;i < n-1;i++)
        printf("%d + ",a[i]);
    printf("%d = %d",a[n-1],sum);    
}
int main()
{
    int n;
    printf("Enter the number of numbers to be added\n");
    scanf("%d",&n);
    int a[n];
    printf("Enter the numbers:\n");
    input(n,a);
    int sum = findSum(n,a);
    print(n,a,sum);
    return 0;
}