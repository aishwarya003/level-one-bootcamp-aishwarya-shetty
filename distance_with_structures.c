//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
typedef struct point
{
    int x; 
    int y;
}points;
points input()
{
    points p;
    printf("Enter x coordinate\n");
    scanf("%d",&p.x);
    printf("Enter y coordinate\n");
    scanf("%d",&p.y);
    return p;
}
float distance(points p1, points p2)
{
    return sqrt(pow((p2.x-p1.x),2)+pow((p2.y-p1.y),2));
}
void print(points p1,points p2,float ans)
{
    printf("Distance between (%d,%d) and (%d,%d) is %.3f",p1.x,p1.y,p2.x,p2.y,ans);
}
int main()
{
    points p1,p2;
    p1 = input();
    p2 = input();
    float ans = distance(p1,p2);
    print(p1,p2,ans);
    return 0;
}