//Write a program to find the volume of a tromboloid using one function
// Online C compiler to run C program online
#include <stdio.h>

int main() {
    // Write C code here
    int h,b,d;
    float vol = 0.0;
    printf("Enter h, b and d\n");
    scanf("%d %d %d",&h,&b,&d);
    vol = 0.333*((h*b*d)+(d/b)); //1/3 = 0.333...
    printf("Volume of tromboloid is %.3f",vol); //rounding of the answer to 3 decimal places
    
    return 0;
}